def kuu_nimi(kuu_number):
    try:
        if int(kuu_number) > 12 and int(kuu_number) < 1:
            print("Halb number!")
        kuu = {1:"jaanuar",2:"veebruar",3:"märts",4:"aprill",5:"mai",6:"juuni",7:"juuli",8:"august",9:"september",10:"oktoober",11:"november",12:"detsember"}
        return kuu[int(kuu_number)]
    except:
        print("Halb sisend!")
        return None
