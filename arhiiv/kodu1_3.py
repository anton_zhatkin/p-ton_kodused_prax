def mediaan(sisend):
    pikk = len(sisend)
    mediaan = 0
    sisend = sorted(sisend)
    if pikk == 1:
        mediaan = sisend[0]
    elif pikk == 2:
        mediaan = (sisend[0]+sisend[1])/2
    elif pikk%2 == 0:
        mediaan = (sisend[int(pikk/2-1)]+sisend[int(pikk/2+1)])/2
    else:
        mediaan = sisend[int((pikk-1)/2+1)]
    return mediaan
