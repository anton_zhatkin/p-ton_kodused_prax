# -*- coding: utf-8 -*-
from os import listdir
from os.path import isdir, join
def uuri_kaust(nimi):
	kaustad = []
	alg = listdir(nimi)
	for asi in alg:
		koht = join(nimi,asi)
		if isdir(koht) == True:
			kaustad.append(uuri_kaust(koht))
		else:
			kaustad.append(asi)
	return kaustad