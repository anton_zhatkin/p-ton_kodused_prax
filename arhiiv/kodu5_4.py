# -*- coding: utf-8 -*-
def konsonandid(vana):
	if vana == "":
		return ""
	valmis = ""	
	täishäälikud = ["a","e","i","o","u","õ","ä","ö","ü"]
	if vana[0] not in täishäälikud:
		valmis += vana[0]
	if len(vana) > 1:
		valmis += konsonandid(vana[1:])
	return valmis
print(konsonandid("ääriveeri"))
print(konsonandid("koorevahukreem"))