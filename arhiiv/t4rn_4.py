# -*- coding: utf-8 -*-
def palk(isapalk,emapalk,lapsi):    
    perepalk = 0
    perepalk += lapsi*20
    if isapalk <= 144:
        perepalk += isapalk
    else:
        perepalk += 144 + (isapalk-144)*0.79
    if emapalk <= 144:
        perepalk += emapalk
    else:
        perepalk += 144 + (emapalk-144)*0.79    
    perepalk = format(perepalk, '.2f')
    return perepalk

from tkinter import *
from tkinter import ttk
from tkinter import messagebox
x = "370"
y = "550"
geomeetria = x + "x" + y
raam = Tk()
raam.title("Pere sissetulek")
raam.geometry(geomeetria)
laius = 180
x = 180
font = ("Sylfaen", 13)

isasilt = ttk.Label(raam, text = "Isa bruto (€):", font = font)
isasilt.place(x = x - 130, y = 15)
isapalk = ttk.Entry(raam)
isapalk.place(x = x, y = 20, width = laius)

emasilt = ttk.Label(raam, text = "Ema bruto (€):", font = font)
emasilt.place(x = x - 130, y = 45)
emapalk = ttk.Entry(raam)
emapalk.place(x = x, y = 50, width = laius)

laps = ttk.Label(raam, text = "Lapsi (tk):", font = font)
laps.place(x = x - 130, y = 75)
lapsi = ttk.Entry(raam)
lapsi.place(x = x, y = 80, width = laius)

sissetul = ttk.Label(raam, text = "Sissetulek (€):", font = font)
sissetul.place(x = x - 130,y = 150)


nupp = ttk.Button(raam, text = "Arvuta!", command = palk)
nupp.place(x = 70, y = 120, width = 250)


raam.mainloop()