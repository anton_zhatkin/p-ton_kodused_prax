# -*- coding: utf-8 -*-
from copy import deepcopy
def loe_sisse(andmed):
    loetud = []    
    with open(andmed,"r") as f:
        loetud = [i.strip().split(";") for i in f.readlines()]
        inimesed = [[g.split("=") for g in i] for i in loetud]
        list1= [[] for i in range(len(inimesed))]
        list2 = deepcopy(list1)
        sõnastik = []
        inimene = -1
        for i in inimesed:
            inimene += 1            
            for g in i:
                list1[inimene].append(g[0])
                list2[inimene].append(g[1])
        for i in range(len(inimesed)):
            sõnastik.append(dict(zip(list1[i],list2[i])))
    return sõnastik
print(loe_sisse("andmed.txt"))