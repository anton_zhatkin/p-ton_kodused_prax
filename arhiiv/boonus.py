from binascii import *
f = open("boonus.txt")
b = f.readlines()   #formateerimata read
print(b)
a = ""              #vahepealne sõne
c = []              #ascii sümboliteks teisendatud asjad
vastus = ""
for i in b:                     #siinkohal panen kõik read üheks
    a = a+i.strip()             #sõneks kokku ja 
for i in range(int(len(a)/8)):  
    c.append((a[i*8:i*8+8]))    

for i in range(len(c)):         #korrutan läbi iga kaheksandiku
    arv = 0                     #vastava kahe astmega ja
    loendur = 7                 #liidan kokku
    for v in range(8):
        arv += int(int(c[i][v])*(2**loendur))
        loendur -= 1
    c[i] = arv
print(c)


for i in range(len(c)):         #kasutan pythoni sisseehitatud
    c[i] = chr(c[i])            #funktsiooni, et teisendada
print(c)                        #numbrid ascii sümboliteks
a = ""
for i in c:
    a += i
print(a)
a = a.split()
vastus = a[0]+ " " + a[1]+ " " + a[2]+ " " + a[3]+ " "
a = a[4]
a = unhexlify(a)                #kasutan pythoni sisseehitatud
a = bytes.decode(a)             #funktsionaalsust, et teisendada
vastus += a                     #kuueteistkümnendarv sõneks
print(vastus)
f.close()
