from turtle import *
from math import cos
from math import pi
laius = int(input("Sisesta ümbriku pikkus!"))
pikkus = int(input("Sisesta ümbriku laius!"))
speed(2)
#delay(0)


forward(pikkus)
right(90)
forward(laius)
a = position()
right(90)
forward(pikkus)
b = position()
right(90)
forward(laius)
goto(a)
forward(laius)
goto(b)
forward(laius)

right(45)
forward(pikkus/2/cos(1/4*pi))
right(90)
forward(pikkus/2/cos(1/4*pi))
exitonclick()
