from pykkar import *


create_world("""
###########
#.........#
#.       .#
#.       .#
#.    >  .#
#.........#
###########
""")
def liigu_seinani():
    while is_wall() == False:
        step()
        
def vasak():
    right()
    right()
    right()
    
suund = get_direction()
if suund == "N" or suund == "E":
    liigu_seinani
    if suund == "N":
        right()
        liigu_seinani()
        vasak()
    else:
        vasak()        
        liigu_seinani()
        right()
        liigu_seinani()
        vasak()
else:
    if suund == "W":
        right()
        liigu_seinani()
        right()
        liigu_seinani()
        vasak()
    if suund == "S":
        right()
        right()
        liigu_seinani()
        right()
        liigu_seinani()
        vasak()
