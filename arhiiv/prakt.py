fail = open(input("sisesta täisarvudega faili nime!"))
fail = fail.readlines()
from turtle import *
up()
forward(-200)
down()
left(90)
def tulp(laius, kõrgus, värv):
    color(värv)
    forward(kõrgus)
    right(90)
    forward(laius)
    right(90)
    forward(kõrgus)
    left(90)
    color("black")
    forward(10)
    left(90)
for i in fail:
    if int(i) > 50:
        v4rv = "red"
    else:
        v4rv = "yellow"
    tulp(50,int(i)*2,v4rv)