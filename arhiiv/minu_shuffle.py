from random import randint


def minu_shuffle(vana):
    uus = []
    while len(vana)>0:
        suvaline = randint(0,len(vana)-1)
        uus.append(vana[suvaline])
        vana.pop(suvaline)

    return uus
