def korrasta_kuupäev(kpäev):
    valmis = ""
    kuu = {1:"jaanuar",2:"veebruar",3:"märts",4:"aprill",5:"mai",6:"juuni",7:"juuli",8:"august",9:"september",10:"oktoober",11:"novmber",12:"detsember"}  
    if "-" in kpäev:
        kpäev = kpäev.split("-")
        valmis = "{0}.{1} {2}".format(kpäev[2],kuu[int(kpäev[1])],kpäev[0])
    elif "." in kpäev:
        kpäev = kpäev.split(".")
        valmis = "{0}.{1} {2}".format(kpäev[2],kuu[int(kpäev[1])],kpäev[0])

    #Ameerika värgid
    elif "/" in kpäev:
        kpäev = kpäev.split("/")
        if kpäev[2] > 15:
            valmis = "{0}.{1} {2}".format(kpäev[1],kuu[int(kpäev[0])],19+kpäev[2])
        else:
            valmis = "{0}.{1} {2}".format(kpäev[1],kuu[int(kpäev[0])],20+kpäev[2])    
    return valmis
