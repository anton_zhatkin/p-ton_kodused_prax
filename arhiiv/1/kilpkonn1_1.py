from turtle import *

def aken(kylg):
    k = kylg
    forward(k)
    left(90)
    forward(k)
    left(90)
    forward(k)
    left(90)
    forward(k)
    up()
    left(90)
    forward(5)
    left(90)
    forward(5)
    down()
    forward(k-10)
    right(90)
    forward(k-10)
    right(90)
    forward(k-10)
    right(90)    
    forward(k-10)
    right(90)
           #skelett
forward(200)
right(90)
b=pos()
forward(270)
                                    ##hiljem lisatud
left(135)
forward(30)
left(45)
forward(270)
a=pos()
goto(-0.00,200.00)
goto(a)
goto(b)
            ##skelett cont.
right(180)
forward(270)
right(90)
forward(400)
right(90)
forward(270)
right(90)
forward(400)
            ##katus aken
left(135)
forward(80000**0.5)
left(90)
forward(80000**0.5)
left(135)
forward(200)
left(90)
up()
forward(20)
right(90)
down()
circle(50)
            ##akna jooned
forward(-5)
left(90)
forward(100)
right(90)
forward(5)
right(90)
forward(100)

up()
right(90)
forward(50)
right(90)
forward(55)
down()
right(90)
forward(100)
right(90)
forward(5)
right(90)
forward(100)
            ##uks 1
left(90)
up()
forward(200)
down()
forward(140)
left(90)
forward(100)
left(90)
forward(60)
            ##ukselink
right(90)
for i in range(4):
    forward(-10)
    aken(10)
            ##uks 2
left(90)
forward(80)
left(90)
forward(100)
            ##aknad
up()
forward(105)
left(90)
forward(45)
left(90)
down()
aken(70)
right(90)
up()
forward(240)
down()
aken(85)

exitonclick()
