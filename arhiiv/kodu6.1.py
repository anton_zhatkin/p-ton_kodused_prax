from turtle import *
speed(10)
delay()
from random import randint
degrees(360)
def joonista(kylgi, pikkus):
    kylg = 180*(kylgi-2)/kylgi
    kylg = 180 - kylg
    for i in range(kylgi):
        forward(pikkus)
        right(kylg)
for i in range(30):
   kohtx = randint(-400,400)
   kohty = randint(-310,260)
   up()
   goto(kohtx,kohty)
   down()
   left(90)
   klgi = randint(3,12)
   pikkus = randint(1,150)
   pikkus = pikkus/(klgi**0.5)
   joonista(klgi,pikkus+40)