# -*- coding: utf-8 -*-
import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import pyqtSlot

app = QApplication(sys.argv)
w = QMainWindow()
w.resize(400, 300)
w.setWindowTitle("Tere nohikud!")


#################################################################
nupp = QPushButton("Hakka netineegriks", w)
nupp.setToolTip("Lükka mind nahhui!")
nupp.clicked.connect(exit)
nupp.resize(nupp.sizeHint())
nupp.move(200, 150)


@pyqtSlot()
def on_press():
    print("pressed")

@pyqtSlot()
def on_click():
    print("clicked")
    QMessageBox.critical(w, msg, "Neegriks ei saada, neegriks sünnitakse!")
    QMessageBox.about(w, "About", "Rohkem info@ kasoledneeger.ee ")

@pyqtSlot()
def on_release():
    print("released")


nupp.clicked.connect(on_click)
nupp.pressed.connect(on_press)
nupp.released.connect(on_release)

#################################################################
msg = "Message"
küssa = "Kas Püüton on parim programmeerimiskeel?"
QMessageBox.warning(w, msg, "Kui sa vastad valesti, siis ma söön su ära!")
QMessageBox.information(w, msg, "Muidu söön ilma soolata, nii et sul on suht sitt olla.")
tekstikast = QMessageBox.question(w, msg, küssa, QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
print(tekstikast)
print(QMessageBox.Yes)
#################################################################


mainMenu = w.menuBar()
mainMenu.setNativeMenuBar(False)
fileMenu = mainMenu.addMenu("&Noos")

exitNupp = QAction(QIcon("exit24.png"), "Läinud", w)
exitNupp.setShortcut("Ctrl+Q")
exitNupp.setStatusTip("Olen noosikuningas.")
exitNupp.triggered.connect(w.close)
fileMenu.addAction(exitNupp)


w.show()
sys.exit(app.exec())
