# -*- coding: utf-8 -*-

#ilmselgelt loob väljaku
def loo_väljak():			 	#hiljem võib lasta suvalst mängustaadiumi panna
	väljak = [[] for i in range(8)]
	for i in range(2,6):
		for g in range(8):
			väljak[i].append(0)	#minu meelest null on parim tähistus tühjale kohale
	väljak[1] = ["e" for i in range(8)];väljak[6] = ["E" for i in range(8)]
	väljak[0] = ["v1", "r1", "o1", "l", "k", "o2", "r2", "v2"]	#mustad nupud väikse tähega
	väljak[7] = ["V1", "R2", "O1", "L", "K", "O2", "R2", "V1"]  #valged suurega
	return väljak

def loo_sõnastik():				#sõnastik tähest numbrisse
	return dict(zip([(chr(97+i)).upper() for i in range(8)], [i for i in range(1,9)]))

def kas_on(mis, kus): 			#kontrollib, kas malend on seal olemas
	try:
		pass
	except:
		return False
	else:
		return True
def ettur(kus, kuhu):
	pass

def mängi():
	sõnastik = loo_sõnastik()
	väljak = loo_väljak()
	print(väljak)
mängi()