from requests import session
import urllib.request
import urllib.parse
try:
	from bs4 import BeautifulSoup
except:
	print("Install BeautifulSoup from folder")

#ENTER USERNAME AND PASSWORD INTO APOSTHROPHES
user = ''
passw = ""

#VALUES
global notdone
notdone = 0

#LOGIN
payload = {
    'action': 'login',
    'username': user,
    'password': passw
}

with session() as c:
    c.post('https://moodle.ut.ee/login/index.php', data=payload)
    response = c.get('https://moodle.ut.ee/my/')

#FILE
f = open("todo.txt", "w")

#GET COURSES
def course_list():
    soup = BeautifulSoup(response.text, 'html.parser')
    for box in soup.findAll("div", { "class" : "box coursebox" }):
        for link in box.findAll('a'):
            if len(link.get('href')) == 43 or len(link.get('href')) == 44:
                url = link.get('href')
                course(url)

#GET TESTS
def course(url):
    global notdone
    response2 = c.get(url)
    soup2 = BeautifulSoup(response2.text, 'html.parser')
    for test in soup2.findAll("div", { "class" : "activityinstance" }):
        if 'Test' in test.get_text():
            for link in test.findAll('a'):
                linkget = link.get('href')
                response2 = c.get(linkget)
                soup2 = BeautifulSoup(response2.text, 'html.parser')
                if soup2.findAll("input", { "value" : "Soorita test" }) != [] or soup2.findAll("input", { "value" : "Jätka viimast katset" }) != []:
                    f.write("SOORITAMATA:\n")
                    f.write("Kursus: "+soup2.title.string+"\n")
                    f.write("Testi nimi: "+test.get_text()+"\n")
                    for link in test.findAll('a'):
                        f.write("Testi link: "+link.get('href')+"\n")
                    if soup2.findAll("div", { "class" : "box quizinfo" }) != []:
                        for p in soup2.findAll("div", { "class" : "box quizinfo" }):
                            f.write(p.getText()+"\n")
                    f.write('-----------\n')
                    notdone += 1


course_list()
f.close()
exit()
